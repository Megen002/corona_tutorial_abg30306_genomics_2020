{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Corona genomics tutorial\n",
    "\n",
    "> Last updated: April 7, 2020; v 0.2.1\n",
    "\n",
    "> Note: This is a new tutorial, whipped together from a number of recent, non-peer reviewed papers, based on data that is not always very well documented. To boot, all of this had to be done in a bit of a hurry. Any mistakes you may spot, - data, code, or text, let me know!\n",
    "\n",
    "> where you can reach me: hendrik-jan.megens --at-- wur.nl\n",
    "\n",
    "### First things first: run the next two code cells\n",
    "\n",
    "Before starting analysis through the notebook, you will have to make sure that you are using the `ABG30306_Genomics/AandA` kernel that you have used for `Module 2`. And furthermore, you need to run the next two code cells every time you restart your server or your kernel.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import some libraries\n",
    "import matplotlib.pyplot as plt\n",
    "from Bio import SeqIO\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set some variables\n",
    "# and copy data to the present working directory\n",
    "import os\n",
    "mypwd = os.getcwd()\n",
    "datadir = '/lustre/shared/ABG30306/Corona_project/data/'\n",
    "os.environ['DATADIR'] = datadir\n",
    "! rsync -av /lustre/shared/ABG30306/Corona_project/files/* $PWD"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Background \n",
    "\n",
    "**Learning objectives:**\n",
    "\n",
    "*At the end of this tutorial, students will be able to:*\n",
    "* recognize the potential for long read sequence data to identify and quantify DNA and RNA in living processes\n",
    "* apply generic genome databases and literature resources to identify and download reference data\n",
    "* apply generic methods for mapping Nanopore sequence reads to study biological properties of SARS-CoV-2\n",
    "\n",
    "Among the major breakthroughs of the past years were the introduction of new sequencing technologies, the most important ones:\n",
    "\n",
    "* Illumina (Sequencing by Synthesis, high quality but short, paired reads\n",
    "* Pacific BioSciences (direct sequencing, long reads, sequencing by real-time analysis of polymerase-based incorporation of fluorescent nucleotides)\n",
    "* Oxford Nanopore (direct DNA molecule sequencing, long reads, measuring base-specific current alterations as molecule is drawn through a pore)\n",
    "\n",
    "The long molecule sequencing technologies have plaid a specific role in understanding the replication cycle of the virus, in much the same way that these technologies are currently revolutionizing Genomics everywhere - cheaper, better characterization of entire genomes (genome assembly), and better characterization of transcriptomes, all by virtue of directly sequencing the molecules of interest, potentially from start to end. In this tutorial we are focussing mostly on Nanopore long reads. In part because one of the key properties of this sequencing technologies is that it is now so widely adopted. In its basic form, Nanopore sequencing is extremely portable. Furthermore, it can generate extremely short turnaround times, from sample prep to sequence data in a matter of hours. Compared to e.g. Illumina sequencing, which typically requires at least several days to result. \n",
    "\n",
    "## Exploring the 'transcriptome' of SARS-CoV-2\n",
    "\n",
    "In this short tutorial, we will investigate how Nanopore long read sequencing has been applied in understanding some of the key properties of COVID-19/SARS-CoV-2. One area where long read, direct RNA molecule, sequencing has made a tremendous impact is in understanding how the RNA genome (gRNA) is expressed in the host cells.\n",
    "\n",
    "To provide the necessary background to interpret results in the tutorial, and to get a better understanding of the \n",
    "the molecular biology and genomics of Corona/SARS like viruses, we recommend the following paper and book chapter:\n",
    "\n",
    "* [Masters, 2006](https://www.sciencedirect.com/science/article/pii/S0065352706660053?via%3Dihub). Not a recent text, but there's loads of information here. In fact, so much you may feel that this is way more than you need. but Just focus on some of the figures and tables (and their legends), they should provide most of the necessary background.\n",
    "* [Fehr and Perlman, 2015 (Chapter 1 of Maier et al., Eds, Coronaviruses, Methods and Protocols)](https://link.springer.com/content/pdf/10.1007%2F978-1-4939-2438-7.pdf). Slightly updated compared to the Masters, 2006 paper, and more readable. While it still has sufficient detail, it lacks good figures to understand some of the fundamentals of replication and transcription.\n",
    "\n",
    "We recommend that you read Fehr and Perlman, 2015 (only first Chapter of the book!), but as you read that, consult the figures and figure legends of Masters, 2006.\n",
    "\n",
    "The first part of the tutorial is mostly based on these two papers:\n",
    "\n",
    "* Kim et al., 2020: https://www.biorxiv.org/content/10.1101/2020.03.12.988865v2.full.pdf\n",
    "* Taiaroa et al., 2020: https://www.biorxiv.org/content/10.1101/2020.03.05.976167v2.full.pdf\n",
    "\n",
    "From **Kim et al.**, study the abstract, Figures 1 and 2, and (STAR) Methods until (not including) \"*DNBseq RNA sequencing*\". \n",
    "\n",
    "From **Taiaroa et al.**, study the abstract, Figure 1, Supplementary Figures 3a, 4, and 7a, and Methods section (especially the definition of the Leader Sequence). \n",
    "\n",
    "The data used for the tutorial is from Kim et al, and can be found [here](https://osf.io/8f6n9/). The data retrieved was in the 'NanoporeDRS/Vero-Infected' directory. *The data does not need to be downloaded again, it is on the Anunna server for you to use.*\n",
    "\n",
    "In the tutorial we will try to replicate a number of steps in the Taiaroa et al. paper, but the data did not lend itself for that very well, most likely due to limited sequence coverage of reads that mapped properly to the reference. The Kim et al. paper however turned out to have excellent data to support not only their own findings, but also the ones from Taiaroa et al. as well. \n",
    "\n",
    "The reference genome we will use is from the first complete SARS-CoV-2 sequence published:\n",
    "* https://www.ebi.ac.uk/ena/data/view/MN908947\n",
    "\n",
    "* https://www.ncbi.nlm.nih.gov/nuccore/MN908947.3?report=genbank\n",
    "\n",
    "(double click cell, then `>|Run` again, if you don't see image below)\n",
    "<img src=\"figures/MN908947_3_annotation.png\" alt=\"fig1\" style=\"width: 700px;\"/>\n",
    "\n",
    "Download the Genbank report for `MN908947.3`.\n",
    "\n",
    "> **Question 1a:** How many open reading frames (ORFs) are found in the SARS-CoV-2 genome? \n",
    "\n",
    "> **Question 1b:** Which is the longest, and how many bases is it? \n",
    "\n",
    "> **Question 1c:** How long is ORF2? How many AAs does the peptide have? \n",
    "\n",
    "> **Question 1d:** According to Kim et al., is the last ORF expressed?\n",
    "\n",
    "> **Question 1e:** What is the feature at the very 3' end?\n",
    "\n",
    "Next we will start investigating and analyzing the data from Kim et al. Before starting analysis through the notebook, you will have to make sure that you are using the `ABG30306_Genomics/AandA` kernel. And furthermore, you need to run the first two code cells, at the start of the notebook, every time you restart your server or your kernel.  \n",
    "\n",
    "First thing we will do is look at the contents of the sequence data. To do that we will look at the first 8 lines, and we will investigate how many sequences there are in total:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show first two sequences\n",
    "! head -8 $DATADIR/VeroInf24h.all.fastq\n",
    "! echo \"Number of sequences:\"\n",
    "! grep 'sampleid=vero-infected' $DATADIR/VeroInf24h.all.fastq | wc -l"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 2a:** How many sequences are displayed in the results above?\n",
    "\n",
    "\n",
    "> **Question 2b:** Is this DNA sequence or RNA sequence data? Looking at the sequencing procedure and the research question, why does this make sense?\n",
    "\n",
    "> **Question 2c:** According to Figure 1 of Kim et al., when sequencing complete RNA molecules, which specific homopolymer is to be expected at the 3' end of the Nanopore sequences? Do you observe this? (take into account that Nanopore sequence data is 'noisy', so some sequence mistakes can occur).\n",
    "\n",
    "> **Question 2d:** How many sequences are there in total in this sequence file?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As above, and in the rest of the tutorial, we will apply approaches you have seen before (for the most part), notably in the Assembly and Annotation tutorial, to demonstrate the general application of sequence-based information. The Next step is to generate a sequence-length distribution, very much as you have done before. For the Assembly and Annotation tutorial, having long sequences was vital for a good genome assembly. But now we are not doing a genome assembly, but rather are looking at 'expressed' RNA molecules from SARS-CoV-2. The code, however, is identical (in fact, a few lines of code had to be removed, that was it):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this part can take up to a few minutes\n",
    "lengths = list()                                             # this list will store all the lengths\n",
    "fastqfile = open(datadir+\"/VeroInf24h.all.fastq\" , 'r')      # open the fastq file with nanopore data\n",
    "\n",
    "for record in SeqIO.parse(fastqfile, \"fastq\"):               # loop through all sequences\n",
    "    lengths.append(len(record.seq))                          # add sequence length to list\n",
    "\n",
    "fastqfile.close()                                            # close the fastqfile\n",
    "\n",
    "print('total bases, summed up from all reads: {:,}'.format(np.sum(lengths)))  # print total number bases"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 3:** What is average sequence coverage, assuming that all reads indeed will align to the SARS-CoV-2 reference genome?\n",
    "\n",
    "And plotting the lengths in the histogram. Again, mostly the same compared to what you've seen before, although we are now scaling the lengths of the reads to make a more presentable histogram. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# and some simple plotting of the histogram\n",
    "plt.figure(figsize=(14, 6))\n",
    "plt.hist(lengths,weights=lengths,bins=1000)                   # plot the histogram\n",
    "plt.xlim([0,10000])\n",
    "plt.title('Distribution of read lengths')\n",
    "plt.xlabel('Read length')\n",
    "plt.ylabel('Total sequence in length category')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will notice a distince 'peaky-ness' of the distribution of reads.\n",
    "\n",
    "> **Question 4:** How do you explain this distribution of read lengths?\n",
    "\n",
    "In both the papers of Kim et al. and Taiaroa et al., you may have notice they've used Minimap2 to align Nanopore reads to the reference genome. You've previously done the same, for *Botrytis aclada* PacBio reads in Module 2, remember!? So, this is a tool that should be somewhat familiar to you. Let's apply it to our current problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# do the mapping - lots of data to map, will take a few minutes\n",
    "! minimap2 -a -x map-ont --secondary=no MN908947_3.fasta $DATADIR/VeroInf24h.all.fastq | samtools view -S -b - >mapped_to_MN908947.unsorted.bam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting alignment file (BAM) is quite big; check it by running the next line of code, it should be at the top of your list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show files in your present working dir\n",
    "# sorted by time created, newest first\n",
    "! ls -lth | head"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And, you may recognize the drill by now, the minimap2 outcome is an alignment file in BAM format, unsorted. So it need sorting, and subsequent indexing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sort and index bam file - impressive alignment, will take a few minutes\n",
    "! samtools sort mapped_to_MN908947.unsorted.bam -o mapped_to_MN908947.sorted.bam\n",
    "! samtools index mapped_to_MN908947.sorted.bam\n",
    "print(\"Housekeeping: remove the unsorted BAM to save space!\")\n",
    "! rm *.sorted.bam.tmp.*.bam            # really just in case samtools sort does not complete correctly\n",
    "                                       # don't worry if throws an error, that just means sorting went ok.\n",
    "! rm mapped_to_MN908947.unsorted.bam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once this is done, we can produce coverage statistics for every base in the reference genome. We use a tool that you have not encountered before during the course, called 'bedtools'. [Bedtools](https://bedtools.readthedocs.io/en/latest/) is the 'Swiss army knife' for everything that you may need to do that involves intervals, such as annotated in .bed or .vcf files, but can also work on .bam files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute coverage - again, patience, takes a few minutes\n",
    "! bedtools genomecov -ibam mapped_to_MN908947.sorted.bam -d >genomecoverage_all_aligned_reads.txt\n",
    "print(\"Coverage calculation done!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output is a three-column file, that holds depth for every base. \n",
    "\n",
    "1. Column 1 is the name of the reference sequence (in this case, for every base, it is MN908947.3)\n",
    "2. the second column is the position in the reference sequence, \n",
    "3. and the third base is the number of reads that are covering that position, or, simply, 'coverage'. \n",
    "\n",
    "Let's look at the first 10 lines of the output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# first 10 lines\n",
    "! head genomecoverage_all_aligned_reads.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Subsequently, we can plot the depth of coverage for every position. We do this by first reading in the three-column table, and then the seconde column is plotted against the third."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read coverage file and plot\n",
    "coverage = pd.read_csv('genomecoverage_all_aligned_reads.txt',sep='\\t',header=None, names=['Ref', 'position','depth'],)\n",
    "coverage.head()\n",
    "plt.figure(figsize=(14, 9))\n",
    "plt.plot(coverage.position, coverage.depth)\n",
    "plt.title('Distribution of coverage')\n",
    "plt.xlabel('Postition in ref genome')\n",
    "plt.ylabel('Depth of coverage')\n",
    "#plt.yscale('log')  # uncomment this line to make the vertical axis log scale"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat the previous code cell by uncommenting the last line of code (i.e. remove the `#`).\n",
    "\n",
    "> **Question 5a:** What is the difference between the two Figures you made, with and without the last line of code active? Why would this be useful to do?\n",
    "\n",
    "Compare the result to `Figure 2B` from Kim et al.\n",
    "\n",
    "> **Question 5b:** What is the interpretation of the 'stepped' appearance of the distribution\n",
    "\n",
    "In Taiaroa et al, the authors were looking for a similar 'stepped' distribution, but from their data it was less apparent compared to the Kim et al. data. To focus on complete mRNAs they selected only the sequenced molecules that contained the specific 'leader' sequence that should be at the start of each 'subgenome' RNA. \n",
    "\n",
    "In the next code cell we create a fasta file, with the file name `leader.fasta`, that contains the leader sequence, which we call `leader_seq`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# leader sequence as described in Taiaroa et al\n",
    "# write it to file: leader.fasta\n",
    "leader = 'ACCUUCCCAGGUAACAAACCAACCAACUUUCGAUCUCUUGUAGAUCUGUUCUCUAAACGAAC'\n",
    "print(len(leader))\n",
    "leader_fh = open('leader.fasta', 'w')\n",
    "leader_fh.write('>leader_seq\\n')\n",
    "leader_fh.write(leader)\n",
    "leader_fh.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can check that it worked by looking at the content of the new fasta file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show contents of 'leader.fasta'\n",
    "! cat leader.fasta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next step, we can map the leader sequence to the Nanopore sequence reads. Let's first just look at how the results look like, by aligning, then selecting only the ones that indeed did align to a leader seq (the third column of the [SAM/BAM](https://samtools.github.io/hts-specs/SAMv1.pdf) output contains the reference sequence name the Nanopore read mapped to). The fourth column of the SAM/BAM output, by the way, shows the position on the reference (we expect that to be close to zero in this case). If you want to get a good idea how well the leader sequence mapped you can have a look at column 6, which shows the CIGAR (**C**oncise **I**diosyncratic **G**apped **A**lignment **R**eport) string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# map and show first lines\n",
    "! minimap2 -a -x map-ont leader.fasta $DATADIR/VeroInf24h.all.fastq | grep leader_seq | head -5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can therefore select only the reads in the SAM/BAM alignment file that were mapped to the leader sequence. Without having to save them seperately, we can simply pass every selected SAM line to a small utility script, `sam_to_fq.py`, that does the conversion on the fly (it simply takes column 1, the read name, column 10, the sequence, and column 11, the quality string and prints them out on separate lines). Just for your reference, the content of the script is below (you have copied the script to your present working directory already, by applying the second code cell).\n",
    "\n",
    "\n",
    "```python\n",
    "import sys\n",
    "\n",
    "counter = 0\n",
    "for line in sys.stdin:\n",
    "    if not line.startswith('@'):\n",
    "        elements = line.rstrip().split('\\t')\n",
    "        readname = elements[0]\n",
    "        readseq = elements[9]\n",
    "        readq = elements[10]\n",
    "        sys.stdout.write(\"@{}\\n{}\\n+\\n{}\\n\".format(readname, readseq,readq))\n",
    "        counter += 1\n",
    "\n",
    "sys.stderr.write(\"Number of sequences written: {}\\n\".format(counter))\n",
    "```\n",
    "\n",
    "So, here it goes, the selected sequences will be transferred to a fastq file called `contain_leader_seq.fq`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# map and create fastq of sequences mapped to leader\n",
    "! minimap2 -a -x map-ont leader.fasta $DATADIR/VeroInf24h.all.fastq | grep leader_seq | python sam_to_fq.py 1>contain_leader_seq.fq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How many sequences were selected this way? (run next code cell)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# how many sequences?\n",
    "! cat contain_leader_seq.fq | grep '^@' | wc -l"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 6:** what percentage of all RNA reads in the dataset actually do contain a leader sequence?\n",
    "\n",
    "Next up, we can do the same as before, map these selected reads, selected to contain the leader sequence, agains the reference genome."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# map, sort, index\n",
    "! minimap2 -a -x map-ont --secondary=no MN908947_3.fasta contain_leader_seq.fq | samtools view -S -b - >mapped_wleader.unsorted.bam\n",
    "! samtools sort mapped_wleader.unsorted.bam -o mapped_wleader.sorted.bam\n",
    "! samtools index mapped_wleader.sorted.bam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we will compute the genome coverage ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute coverage\n",
    "! bedtools genomecov -ibam mapped_wleader.sorted.bam -d >genomecov_wleader.txt\n",
    "print(\"Done computing coverage!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and again, we will plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read coverage and plot\n",
    "coverage = pd.read_csv('genomecov_wleader.txt',sep='\\t',header=None, names=['Ref', 'position','depth'],)\n",
    "coverage.head()\n",
    "plt.figure(figsize=(14, 9))\n",
    "plt.plot(coverage.position, coverage.depth)\n",
    "plt.title('Distribution of coverage')\n",
    "plt.xlabel('Postition in ref genome')\n",
    "plt.ylabel('depth')\n",
    "plt.yscale('log')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare the result to Taiaroa et al., Supplementary Figure 7.\n",
    "\n",
    "> **Question 7a:** Explain the difference in the coverage distribution compared to the previous coverage distribution you made (i.e. Question 5). \n",
    "\n",
    "> **Question 7b:** Identify the open reading frames.\n",
    "\n",
    "Now download `mapped_wleader.sorted.bam` and `mapped_wleader.sorted.bam.bai` to your own computer, and start up `IGV`. For genome, upload the Genbank report for `MN908947.3` (see Question 1). IGV can detect both the sequence and the annotations directly, if the genbank report is used as reference genome (go to \"Genomes\" --> \"Load Genome from File\". \n",
    "\n",
    "Then also load the `mapped_wleader.sorted.bam` alignment file. You may notice that by default not many reads are show. The read depth can at places get far too high to display. IGV by default downsamples the reads to display. But because there is such a massive difference in read depth accross the SARS-CoV-2 genome, this does not work very well for especially ORF1. Go to \"View\" --> \"Preferences\" --> \"Alignments\" (which is one of the tabs) --> set the tick box for 'Downsample reads', and sampling window size to '50' and number of reads per window to '100'. Don't forget to 'Save'.\n",
    "\n",
    ">**Question 8a:** Identify the ORFs and discrete transitions in read depth from your IGV genome browser.\n",
    "\n",
    ">**Question 8b:** Zoom in on the junction between ORF8 and ORF9. What lies between these ORFs? \n",
    "\n",
    ">**Question 8c:** Do the RNA reads start before ORF9, or at the start/after ORF9? \n",
    "\n",
    ">**Question 8d:** Do all ORF9-spanning reads start at the same position? \n",
    "\n",
    "Finally we can again look at the distribution of read lengths. From 'eyeballing' the IGV alignments you will have noticed that the reads usually start and end at very similar positions. So, after only selecting the sequence reads that do contain the leader sequence, how does the length distribution look like?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create length distribution of sequence reads, and plot\n",
    "lengths = list()                                     # this list will store all the lengths\n",
    "fastqfile = open(\"contain_leader_seq.fq\" , 'r')      # open the fastq file with nanopore data\n",
    "counter = 0\n",
    "for record in SeqIO.parse(fastqfile, \"fastq\"):       # loop through all sequences\n",
    "    lengths.append(len(record.seq))                  # add sequence length to list\n",
    "    if len(record.seq) > 29000:\n",
    "        counter += 1\n",
    "fastqfile.close()                                    # close the fastqfile\n",
    "\n",
    "print(counter)\n",
    "print('total bases in all reads: {:,}'.format(np.sum(lengths)))     # print total number bases\n",
    "print('number of sequence reads > 29,000 bp: {}'.format(counter))           # print number reads >29kb\n",
    "\n",
    "\n",
    "# and some simple plotting of the histogram\n",
    "plt.figure(figsize=(12, 6))\n",
    "plt.hist(lengths,weights=lengths,bins=1000)          # plot the histogram\n",
    "plt.xlim([0,10000])\n",
    "plt.title('Distribution of read lengths')\n",
    "plt.xlabel('Read length')\n",
    "plt.ylabel('bases in read length class')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 9a:** What is different and what is the same in the distribution of read lengths above, compared to the one you made earlier (i.e. Question 4)?\n",
    "\n",
    "> **Question 9b:** Identify which reads belong to which ORF (see also Taiaroa et al., Supplementary Figure 3)\n",
    "\n",
    ">> **Question 9c:** Why would it be of interest to specifically report the reads that are longer than 29,000 bp? (note: these are not in the figure, but are reported separatedly above.)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## COVID-19 Diagnostics - applying Nanopore reads. The short of it\n",
    "\n",
    "Testing for COVID-19 has become a hotly debated issue - WHO states that the best strategy is to test as much as possible to be able to find infected people, even if asymptomatic, and isolate them until they recover/are no longer infectious. Most diagnostic tests simply provide a 'yes'/'no' answer regarding the presence of viral particles, usually from nose swabs or saliva. Such tests can, e.g., be based on PCR that use viral gRNA specific primers. \n",
    "\n",
    "However, it is of vital importance to also track the evolution of the virus. For that we can apply the sequence, and investigate whether mutations have arisen. Not only can give this insight in signs that the virus may start behaving differently, but it has also been an incredible tool to track the [migration of the virus accross the globe](https://nextstrain.org/ncov). Every day, new sequences are added to global databases, in a truly collaborative way that demonstrates the value of doing 'open science'. \n",
    "\n",
    "As with everything in dealing with this virus, time/speed are of the essence. While bureaucratic entities, set up to deal with emerging diseases no less, throughout many countries have dropped the ball, scientists are now racing to better understand the disease in every aspect that is relevant, delivering results in a matter of days. \n",
    "\n",
    "One sequence-based technology that is highly suited for quick turn-around of results, is Oxford Nanopore. Their technology can result in a complete viral gRNA sequence, from collection to final result, [in a mere 7 hours!](https://nanoporetech.com/about-us/news/novel-coronavirus-covid-19-information-and-updates). The sequencing itself only lasting about an hour.  \n",
    "\n",
    "Much of these protocols were developed by a wider scientific community. Among the widely used procedures was developed by the [ARTIC network](https://artic.network/ncov-2019).\n",
    "\n",
    "The procedures for diagnostics are very different from the data collection used for the first part of the tutorial, which is based on cultured and 'growing' virus particles. In stead, diagnostics is a much more messy afair, where virus particles, most no longer 'growing', but rather presented as excreted viral particles, have to be collected from the uppermost respiratory tract (nose, mouth). Sampling usually results in virus particles being together with a lot of host DNA/RNA. \n",
    "\n",
    "The protocol designed by the ARTIC network can be found here:\n",
    "\n",
    "* https://www.protocols.io/view/ncov-2019-sequencing-protocol-bbmuik6w\n",
    "\n",
    "There first is an RT-PCR step applying random hexamer primers. The virus genome is subsequently amplified by PCR by applying a coctail of primers that result in partially overlapping fragments. \n",
    "\n",
    "The primer set can be found here:\n",
    "\n",
    "* https://github.com/artic-network/artic-ncov2019/blob/master/primer_schemes/nCoV-2019/V3/nCoV-2019.bed\n",
    "\n",
    "We will look at a dataset from a patient in California that was sequenced by the Andersen lab at the Scripps Institute in California, USA. The description of the case and procedures for sequencing can be found here: \n",
    "\n",
    "* https://andersen-lab.com/secrets/data/hcov-19-genomics/\n",
    "\n",
    "The dataset can be found here:\n",
    "* https://www.ncbi.nlm.nih.gov/sra/SRX7949318[accn]\n",
    "\n",
    "\n",
    "The amount of sequence data was definitely overkill for the purpose. To make the dataset a bit more manageable for this tutorial, the data was (randomly) downsampled, at around 1/30th.\n",
    "\n",
    "The sequence data can be found in the file `PC00101P_L1_downsampled.fastq`. Let's have a quick look at the first three sequences:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show first three sequences of fastq file\n",
    "! head -12 PC00101P_L1_downsampled.fastq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 10a:** Is the sequence data DNA or RNA\n",
    "\n",
    "> **Question 10b:** Why does this make sense?\n",
    "\n",
    "First let's have a look at the length distribution of the sequence reads:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create length distribution of sequence reads and plot\n",
    "lengths = list()                                           # this list will store all the lengths\n",
    "fastqfile = open(\"PC00101P_L1_downsampled.fastq\" , 'r')    # open the fastq file with nanopore data\n",
    "for record in SeqIO.parse(fastqfile, \"fastq\"):             # loop through all sequences\n",
    "    lengths.append(len(record.seq))                        # add sequence length to list\n",
    "\n",
    "fastqfile.close()                                          # close the fastqfile\n",
    "\n",
    "print('total bases: {:,}'.format(np.sum(lengths)))           # print total number bases\n",
    "\n",
    "# and some simple plotting of the histogram\n",
    "plt.figure(figsize=(12, 6))\n",
    "plt.hist(lengths,bins=1000)                                # plot the histogram\n",
    "plt.xlim([0,2500])\n",
    "plt.title('Distribution of read lengths')\n",
    "plt.xlabel('Read length')\n",
    "plt.ylabel('Read count')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 11a:** What is the size of the largest peak?\n",
    "\n",
    "> **Question 11b:** Nanopore can sequence very long reads. Why then are the sequence reads here so short?\n",
    "\n",
    "> **Question 11c:** When looking at the [primer set used](https://github.com/artic-network/artic-ncov2019/blob/master/primer_schemes/nCoV-2019/V3/nCoV-2019.bed), why does the read length of the highest peak make sense?\n",
    "\n",
    "> **Question 11d:** Apart from the highest peak, there are addition peaks. What strikes you about the interval of each of these peaks? What could be the cause? (Hint: chimeric reads)\n",
    "\n",
    "And, very similar to earlier parts of the exercise, we can now map to the reference genome...:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# map, sort, index\n",
    "! minimap2 -a -x map-ont MN908947_3.fasta PC00101P_L1_downsampled.fastq | samtools view -S -b - >PC00101P.unsorted.bam\n",
    "! samtools sort PC00101P.unsorted.bam -o PC00101P.sorted.bam\n",
    "! samtools index PC00101P.sorted.bam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and then, again, the same steps to compute coverage over the genome and plot it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute coverage\n",
    "! bedtools genomecov -ibam PC00101P.sorted.bam -d >PC00101P_coverage.txt\n",
    "print(\"Coverage stats completed!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# read coverage stats and plot\n",
    "coverage = pd.read_csv('PC00101P_coverage.txt',sep='\\t',header=None, names=['Ref', 'position','depth'],)\n",
    "coverage.head()\n",
    "plt.figure(figsize=(14, 5))\n",
    "plt.plot(coverage.position, coverage.depth)\n",
    "plt.title('Distribution of coverage')\n",
    "plt.xlabel('Postition in ref genome')\n",
    "plt.ylabel('depth')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 12a**: The distribution has a very discrete, 'blocky' appearance. Why is it so 'blocky', and, on average, what is the size of each 'block'?\n",
    "\n",
    "> **Question 12b:** Here too we see a big difference in coverage over the genome. It is however, not the same degree of difference in coverage compared to the direct RNA sequencing of the first part of the tutorial. What is the cause of the uneven distribution?\n",
    "\n",
    "You will have noted that many of the tools got repeated time and again, and most were familiar from the Assembly and Annotation tutorial. As noted above, this is by design, since we want you to realize that much of the work on COVID-19/SARS-CoV-2 is rooted in the same molecular biology, bioinformatics, and genomics that you've encountered earlier in our Genomics cours. \n",
    "\n",
    "We hope this tutorial has been a good introduction and motivation for the next and final phase of the Genomics course: the Corona literature subjects.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ABG30306_Genomics/AandA",
   "language": "python",
   "name": "aanda"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
